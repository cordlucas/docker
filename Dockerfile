FROM python:3.12-alpine
WORKDIR /api
RUN pip3 install --upgrade pip && \ 
    pip3 install requests
COPY ./api .
CMD [ "python3", "api.py" ]